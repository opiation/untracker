/*global
    window
*/

(function () {
    "use strict";

    const tokens = {
        fragment: "#",
        query: "?",
        joiner: "&",
        assignment: "=",
        scheme: "://",
        pathname: "/",
        port: ":",
        user: "@"
    };

    function objectifyURL(href) {
        if (typeof href !== "string") {
            throw new TypeError("URL must be a string to be broken up");
        }

        const url = {
            href: href
        };
        let remaining = href;

        const fragmentIndex = remaining.indexOf(tokens.fragment);
        if (fragmentIndex > -1) {
            url.fragment = remaining.slice(fragmentIndex + 1);
            remaining = remaining.slice(0, fragmentIndex);
        }

        const queryIndex = remaining.indexOf(tokens.query);
        if (queryIndex > -1) {
            url.query = remaining.slice(queryIndex + 1);
            url.parameters = url.query.split(tokens.joiner).map(function (parameter) {
                return parameter.split(tokens.assignment).map(window.decodeURIComponent);
            });
            remaining = remaining.slice(0, queryIndex);
        }

        const schemeIndex = remaining.indexOf(tokens.scheme);
        if (schemeIndex > -1) {
            url.scheme = remaining.slice(0, schemeIndex);
            remaining = remaining.slice(schemeIndex + tokens.scheme.length);
        }

        const userIndex = remaining.indexOf(tokens.user);
        if (userIndex > -1) {
            url.user = remaining.slice(0, userIndex);
            remaining = remaining.slice(userIndex + 1);
        }

        const pathnameIndex = remaining.indexOf(tokens.pathname);
        if (pathnameIndex > -1) {
            url.pathname = remaining.slice(pathnameIndex);
            remaining = remaining.slice(0, pathnameIndex);
        }

        const portIndex = remaining.indexOf(tokens.port);
        if (portIndex > -1) {
            url.port = Number.parseInt(remaining.slice(portIndex + 1), 10);
            remaining = remaining.slice(0, portIndex);
        }

        url.domainname = remaining;

        return url;
    }

    function stringifyURL(url) {
        if (url === undefined || url === null) {
            throw new TypeError("Cannot get URL string from null or undefined");
        }

        return url.href;
    }

    window.objectifyURL = objectifyURL;
    window.stringifyURL = stringifyURL;
}());

"use strict";
const keyValueJoiner = "=";
const pairJoiner = "&";



function keyValuePairFromParameter(parameter) {
    if (parameter.length === 0) {
        //console.warn("Parameter is empty\nReturning empty key/value pair");
        return [];
    }

    let pair = parameter.split(keyValueJoiner);
    if (pair.length === 0) {
        //console.warn("Found 0 keys/values in parameter %s\nReturning empty pair", pair.length, parameter);
        return [];
    }

    if (pair.length === 1) {
        return pair.concat("");
    }

    if (pair.length > 2) {
        //console.warn("Found %d key/values in parameter %s\nUsing only the first 2", pair.length, parameter);
        pair = pair.slice(0, 2);
    }

    const decodedPair = pair.map(window.decodeURIComponent);

    //console.debug("Got decoded pair %O from parameter %s", decodedPair, parameter);
    return decodedPair;
}

function keyValuePairsFromQueryString(queryString) {
    if (queryString.length === 0) {
        //console.warn("Query string is empty\nReturning 0 key/value pairs");
        return [];
    }

    const parameters = queryString.split(pairJoiner);
    if (parameters.length === 0) {
        //console.warn("Found 0 parameters in query string %s\nReturning - key/value pairs", queryString);
        return [];
    }

    const keyValuePairs = parameters.map(keyValuePairFromParameter);

    //console.debug("Decoded %d key/value pairs from query string %s\n%O", keyValuePairs.length, queryString, keyValuePairs);
    return keyValuePairs;
}

function parameterFromKeyValuePair(pair) {
    if (pair.length < 2) {
        //console.warn("Found %d keys/values in key value pair\nReturning empty parameter string", pair.length);
        return "";
    }

    const encodedPair = pair.map(window.encodeURIComponent);

    const parameter = encodedPair.join(keyValueJoiner);

    //console.debug("Got parameter %s from key/value pair %O", parameter, pair);
    return parameter;
}

function queryStringFromKeyValuePairs(pairs) {
    if (pairs.length === 0) {
        //console.warn("Found %d key/value pairs\nReturning empty query string", pairs.length);
        return "";
    }

    const encodedParameters = pairs.map(parameterFromKeyValuePair);

    const queryString = encodedParameters.join(pairJoiner);

    //console.debug("Got query string %s from key/value pairs %O", queryString, pairs);
    return queryString;
}
