/*global
    chrome, document, window
*/

(function () {
    "use strict";
    console.group("Load untrack.js");

    const wrappers = [
        {
            matchURL: /^https?:\/\/(www\.)?google\.com\/url/i,
            sanitize: function (href) {
                const queryStringIndex = href.indexOf("?");
                if (queryStringIndex === -1) {
                    console.warn("Could not find destination URL from refirect %s", href);
                    return href;
                }

                const decodedPairs = keyValuePairsFromQueryString(href.substring(queryStringIndex + 1));
                if (decodedPairs.length === 0) {
                    console.warn("Found no decoded key/value pairs in redirect %s", href);
                    return href;
                }

                const destinationPair = decodedPairs.find(function (pair) {
                    return pair[0] = "q" || pair[0] === "Q";
                });
                if (destinationPair === undefined) {
                    console.warn("Could not find destination URL q from redirect %s", href);
                    return href;
                }

                const sanitizedHref = pair[1];
                return sanitizedHref;
            }
        }
    ];
    const trackerTokens = [
        {
            matchKey: /^utm_.*$/i
        }
    ];

    // Get all anchors
    const anchors = Array.from(document.querySelectorAll("a"));
    console.log("Found %d anchor(s) in the document\n%O", anchors.length, anchors);

    const anchorsWithHref = anchors.filter(function anchorHasHref(anchor) {
        return anchor.href.length > 0;
    });
    console.log("Found %d anchor(s) with links in the document", anchorsWithHref.length, anchorsWithHref);

    console.group("Href sanitization");
    const changes = anchorsWithHref.reduce(function (thusFar, anchor) {
        const href = anchor.href;

        const matchingWrapper = wrappers.find(function (wrapper) {
            return wrapper.matchURL.test(href);
        });
        if (matchingWrapper !== undefined) {
            const sanitizedHref = matchingWrapper.sanitize(href);
            anchor.href = sanitizedHref;
            console.info("Anchor with href %s was found to match %s\nReplaced href\n%s => %s", href, matchingWrapper.matchURL, href, sanitizedHref);
            return thusFar.concat({
                oldValue: href,
                newValue: sanitizedHref
            });
        }

        const queryIndex = href.indexOf("?");
        if (queryIndex === -1) {
            //console.log("%s did not match any known wrapper format or contain any query", href);
            return thusFar;
        }

        const queryParameters = keyValuePairsFromQueryString(href.substring(queryIndex + 1));
        if (queryParameters.length === 0) {
            //console.log("%s did not match any known wrapper format or contain any token", href);
            return thusFar;
        }

        const remaining = queryParameters.reduce(function (keeping, pair) {
            const matchingTrackerToken = trackerTokens.find(function (trackerToken) {
                return trackerToken.matchKey.test(pair[0]);
            });
            if (matchingTrackerToken !== undefined) {
                console.info("Query key name %s was found to match known tracker token %s\nRemoving tracker token from URL", pair[0], matchingTrackerToken.matchKey);
                return keeping;
            }

            //console.log("Query key name %s did not match any known tracker token\nKeeping query parameter '%s=%s'", pair[0], pair[0], pair[1]);
            return keeping.concat([pair]);
        }, []);

        if (remaining.length === queryParameters.length) {
            //console.log("%s did not match any known wrapper format or contain any tracking tokens", href);
            return thusFar;
        }

        if (remaining.length === 0) {
            const sanitizedHref = href.substring(0, queryIndex);
            anchor.href = sanitizedHref;
            console.log("Found %d tracking tokens\nReplaced href\n%s => %s", queryParameters.length - remaining.length, href, sanitizedHref);
            return thusFar.concat({
                oldValue: href,
                newValue: sanitizedHref
            });
        }

        const sanitizedHref = href.substring(0, queryIndex + 1) + queryStringFromKeyValuePairs(remaining);
        anchor.href = sanitizedHref;
        console.log("Found %d tracking tokens\nReplaced href\n%s => %s", queryParameters.length - remaining.length, href, sanitizedHref);
        return thusFar.concat({
            oldValue: href,
            newValue: sanitizedHref
        });
    }, []);
    if (changes.length > 0) {
        console.log("Made %d change(s) to anchors in this document\n%O", changes.length, changes);
    }
    console.groupEnd();

    console.groupEnd();
}())
