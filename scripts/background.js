/*global
    chrome, console, Promise, window
*/

"use strict";

console.debug("Started background.js");

const api = {
    appendChanges: function (changes, sender) {
	if (sender.tab === undefined) {
	    return Promise.reject("Changes are only accepted from tab content scripts.");
	}

	const storageId = "tab-" + sender.tab.id;
	const defaultResult = {
	    [storageId]: []
	};

	return Promise.resolve(defaultResult)
	    .then(chrome.storage.local.get)
	    .then(function (result) {
		return {
		    [storageId]: result[storageId].concat(Array.isArray(changes) ? changes : [changes])
		};

		//return newResult;
	    })
	    .then(chrome.storage.local.set)
    },
    clearChanges: function (tabId) {
	if (typeof tabId !== "number") {
	    return Promise.reject("Changes are only  from tab scripts");
	}

	const storageId = "tab-" + tabId;

	return chrome.storage.remove(storageId);
    },
    listChanges: function (tabId) {
	if (typeof tabId !== "number") {
	    return Promise.reject("Tab ID must be a number to fetch all sanitized URLs");
	}

	const storageId = "tab-" + tabId;
	const defaultResult = {
	    [storageId]: []
	};
	
	return Promise.resolve(defaultResult)
	    .then(chrome.storage.local.get)
	    .then(function (result) {
		return result[storageId];
	    });
    }
};

function messageHandler(message, sender, respond) {
    if (message === undefined) {
	// Ignore empty messages
	return;
    }

    if (typeof message.type !== "string") {
	// The message type is used as an API name
	// If there is no valid string API name, there can be no valid API
	return;
    }

    const method = api[message.type];
    if (typeof method !== "function") {
	// Not a valid API method
	return;
    }

    method(message.content, sender)
	.then(respond);

    return true;
}
