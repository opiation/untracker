/*global
    chrome, Promise
*/

"use strict";

chrome.runtime.sendMessage = promisify(chrome.runtime.sendMessage, chrome.runtime);
chrome.storage.local.get = promisify(chrome.storage.local.get, chrome.storage.local);
chrome.storage.local.remove = promisify(chrome.storage.local.remove, chrome.storage.local);
chrome.storage.local.set = promisify(chrome.storage.local.set, chrome.storage.local);
chrome.tabs.query = promisify(chrome.tabs.query, chrome.tabs);
