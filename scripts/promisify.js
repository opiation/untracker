/*global
    Array, Promise
*/

"use strict";

function promisify(fn, context) {
    return function (/* args */) {
	const args = Array.from(arguments);

	if (typeof args[args.length - 1] === "function") {
	    return fn.apply(context, args);
	}

	return new Promise(function (resolve, reject) {
	    args.push(function (result) {
		if (chrome.runtime.lastError !== undefined) {
		    return reject(chrome.runtime.lastError);
		}
		
		return resolve(result);
	    });

	    return fn.apply(context, args);
	});
    };
}
