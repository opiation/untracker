# Untracker

Untracker is a small Chrome browser extension to remove tracking tokens from links and unwrap redirect URLs used for tracking like **www.google.com/url**.

## Tracking tokens
Untracker removes UTM tokens starting with `utm_`.

## Redirect URLs
Untracker finds Google redirect URLs like `www.google.com/url?q=facebook.com` and unwraps them leading directly to the desired URL like `facebook.com`.  This has the advantage of saving a redirect which can reduce latency and network usage.
